OBJECTS = main.o tab.o
EXEC = truc
FLAGS = -Wall
CC = g++

$(EXEC): $(OBJECTS)
	$(CC) $(FLAGS) -o $(EXEC) $(OBJECTS)

%.o: %.cc
	$(CC) $(FLAGS) -o $@ -c $<
