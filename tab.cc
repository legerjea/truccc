/* Copyright 2019, Jean-Benoist Leger <jbleger@hds.utc.fr>,
 *                 Université de Technologie de Compiègne, France
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "tab.h"

bool OPair::operator<(const OPair & oth) const
{
    if(a < oth.a)
        return true;
    if(a > oth.a)
        return false;
    return (b<oth.b);
}

typedef std::map<OPair,int> Tableau;

void read_file(const std::string & filename, Tableau & tab)
{
    std::ifstream inputfile(filename);
    for(
            std::string line;
            std::getline(inputfile, line);
       )
    {
        std::istringstream linestream(line);
        int i1,i2,i3;
        std::string readed;
        std::getline(linestream, readed, '\t');
        i1 = std::stoi(readed);
        std::getline(linestream, readed, '\t');
        i2 = std::stoi(readed);
        std::getline(linestream, readed, '\t');
        i3 = std::stoi(readed);
        tab[OPair(i1,i2)] = i3;
    }
}

void print_tab(const Tableau & tab)
{
    for(
            Tableau::const_iterator it = tab.begin();
            it != tab.end();
            it++
       )
    {
        std::cout << "(" << it->first.a << "," << it->first.b << ") -> " << it->second << "\n";
    }
}

