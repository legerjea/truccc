# Quoi

Ceci est un exemple rapide de code C++ utilisant la STL, avec une définition de
classe, une lecture de fichier, et une compilation.

Ce code lit dans un fichier d'entrée (`a.txt`) des triplet d'entiers séparés par
des tabulations. Les deux premières colonnes sont la clef, et la troisième la
valeur. Les données sont insérés dans une structure de données, et affichés
après insertion.

# Compilation

Se placer dans le repertoire, et lancer `make`

# Execution

Se placer dans le repertoire, et lancer `./truc`
